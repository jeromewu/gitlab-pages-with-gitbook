# gitlab-pages-with-gitbook

Gitbook is a good way to maintain technical document, few highlights for its capabilities:

- Navigation
- Customizable theme
- Various plugins
- Live preview in development time
- Full-text search

## Show cases

**Syntax Highlight**

```javascript
const foo = () => {
  console.log('bar');
}
```
